# HTTP Server Shell
# Author: Barak Gonen
# Purpose: Provide a basis for Ex. 4.4
# Note: The code is written in a simple way, without classes, log files or other utilities, for educational purpose
# Usage: Fill the missing functions and constants

# TO DO: import modules
import os
import socket
import random
import os.path
import sys
import math
random.seed(a=None)
TRAIN_DATA = []
TRAIN_RESULTS = []
CHILDREN = 20
GEN_COUNTER = 0
GENERATIONS = 3000
COUNTER = 0
NETWORKS = []
NEURONS = [2, 5, 1]  # the amount of neurons in each network's layer
TURNS_THIS_MAX = 0
MAX_FITNESS = 0

IP = '127.0.0.1'
PORT = 5000     # might have to be 84 / 80
SOCKET_TIMEOUT = 20000

DEFAULT_URL_HEADER = 'http://www'
DEFAULT_URL = 'fluffy.html'

VERSION = "HTTP/1.1"
OK = '200 OK'
PAGE_NOT_FOUND_SCRIPT = '<DOCTYPE html><html><head style = italic>PAGE NOT FOUND, thanks to you</head></html>'
GET_INDEX = 0
URL_INDEX = 1
VERSION_INDEX = 2
RECEIVE_LENGTH = 1024
MAX_CONNECTIONS = 10
LAST_RESOURCE = ''

"""
INSTRUCTIONS:
To run the project, first, run this server.
Then, go to the browser, and type in the url box: 127.0.0.1:5000
That will connect between the browser and the server, and then the game will begin.
"""


class Neuron:
    def __init__(self, entries):
        self.self_multiplier = 1.0
        self.inputs = entries  # a list of all Neuron inputs
        self.weights = []  # a list of all weights.
        self.value = 0.0
        for v in xrange(len(self.inputs)):
            self.weights.append(0.0)


# a change in a network
class Change:
    def __init__(self):
        self.layer = -1
        self.neuron = 0
        self.input = 0
        self.add = 0.0

    def show(self):
        print '{', self.layer, ' : ', self.neuron, ' : ', self.input, ' : ', self.add, '}'


class Network:
    def __init__(self, network):
        self.layers = network
        self.fitness = 0

    def add_change(self, change):
        self.layers[change.layer][change.neuron].weights[change.input] += change.add


def translate_into_text(network):
    header = '['
    net_str = ''
    for layer in network.layers:
        header += str(len(layer)) + ','
        for neuron in layer:
            net_str += str(neuron.self_multiplier) + ':' + str(neuron.weights) + ';'
        net_str = net_str[:-1] + '/'
    header = header[:-1] + ']'
    net_str = '{' + net_str[:-1] + '}'
    return header + net_str


def translate_into_network(txt):
    header = txt[txt.find('[') + 1:txt.find(']')].split(',')
    for row in xrange(len(header)):
        header[row] = int(header[row])
    new_net = initialize_network(header)    # initialize network according to header
    layers = txt[txt.find('{') + 1:txt.find('}')].split('/')    # break body expression to network layers
    for layer in xrange(len(layers)):   # iterate through each layer
        neurons = layers[layer].split(';')  # break each layer into it's neurons
        for neuron in xrange(len(neurons)):     # iterate through each neuron in iterated layer
            new_net.layers[layer][neuron].self_multiplier = float(neurons[neuron].split(':')[0])
            weights = neurons[neuron].split(':')[1]
            weights = weights[weights.find('[') + 1:weights.find(']')].split(', ')
            if layer > 0:
                for row in xrange(len(weights)):
                    weights[row] = float(weights[row])
                new_net.layers[layer][neuron].weights = weights
    return new_net


# updates a neuron and all of it's inputs (recursive)
def update_value(neuron):
    result = 0.0
    for input in neuron.inputs:
        if len(input.inputs) == 0:
            input_orig_value = input.value
        else:
            input.value = update_value(input)
            input_orig_value = input.value
        result += input_orig_value * neuron.weights[neuron.inputs.index(input)]
    return int(result)


# returns a neural network build following an array of layer sizes
def initialize_network(neurons):
    layers = []  # layers list
    # fill first layer (input layer)
    for i in xrange(len(neurons)):  # initialize the layers list
        empty = []
        layers.append(empty)
    for i in xrange(neurons[0]):  # make first layer
        empty = []
        new_input = Neuron(empty)
        layers[0].append(new_input)
    for l in xrange(1, len(neurons)):  # make all other layers
        for i in xrange(neurons[l]):
            layers[l].append(Neuron(layers[l - 1]))
    return Network(layers)


# updates the values of all neurons in a network, according to a new input
def update_network(network, inputs):
    for i in xrange(len(network[0])):  # update the first layer
        network[0][i].value = inputs[i]
    for i in xrange(len(network[-1])):
        network[-1][i].value = update_value(network[-1][i])
    return network


# print the network
def print_network(network):
    for layer in network.layers:
        row = ''
        for neuron in layer:
            row += str(neuron.value) + ' '
        print row


# creates a random change
def create_random_change(network):
    change = Change()
    change.layer = random.randint(1, len(network)-1)
    if len(network[change.layer]) > 0:
        change.neuron = random.randint(0, len(network[change.layer]) - 1)
        if len(network[change.layer][change.neuron].inputs) > 0:
            change.input = random.randint(0, len(network[change.layer][change.neuron].weights) - 1)
            spot = math.ceil(random.randint(-10, 10)) / 10.0
            change.add = spot
    return change


# temporarily inserts a random change to the network, checks it's fitness, and then returns the fitness and the change
def make_network_change(network, change):
    net = Network(network)
    net.add_change(change)
    return net


def copy_network(network):
    return translate_into_network(translate_into_text(network))


def arrange_by_fitness(networks):
    new_list = []
    empty = []
    for i in xrange(len(networks)):
        best = Network(empty)
        for net in networks:
            if net.fitness >= best.fitness and net not in new_list:
                best = net
        new_list.append(best)
    return new_list


def make_generations():
    global NETWORKS
    global MAX_FITNESS
    global TURNS_THIS_MAX
    NETWORKS = arrange_by_fitness(NETWORKS)     # arrange the networks by fitness
    if NETWORKS[0].fitness == MAX_FITNESS:
        TURNS_THIS_MAX += 1
        if TURNS_THIS_MAX >= 50:
            TURNS_THIS_MAX = 0
            MAX_FITNESS = 0
            create_first_generation()
            return
    elif NETWORKS[0].fitness > MAX_FITNESS:
        MAX_FITNESS = NETWORKS[0].fitness
        TURNS_THIS_MAX = 0
    for i in xrange(CHILDREN-1):#xrange(int(math.floor(CHILDREN / 2))):
        net = Network(copy_network(NETWORKS[0]).layers)
        changes = random.randint(0, 2)
        for j in xrange(changes):
            net = make_network_change(net.layers, create_random_change(net.layers))
        """while translate_into_text(net) in list(map(lambda x: translate_into_text(x), NETWORKS)):
            net = make_network_change(copy_network(NETWORKS[0]).layers, create_random_change(copy_network(NETWORKS[0]).layers))"""
        NETWORKS[i+1] = net


def get_file_data(filename):
    """ Get data from file """
    try:
        print filename
        file_open = open(filename, 'r')
        data = file_open.read()
        file_open.close()
        return data
    except IOError:
        print('error while reading the file')
        return '404 NOT FOUND'


def get_file_type(url):
    return url[url.find('.') + 1:]  # take the string after the period.


def get_status_code():
    return OK


def play_turn(message):
    response = ''
    bots_input = message.split(',')     # split into each bot's info
    for input in bots_input:
        bot_id = int(input.split('=')[0])    # split between bot id and parmeters
        if 'DEAD' not in input.split('=')[1]:
            inputs = (list(map(lambda x: float(x), input.split('=')[1].split(':'))))
            network = update_network(NETWORKS[int(bot_id)].layers, inputs)
            max_imal = 0
            for result in xrange(len(network[-1])):
                if network[-1][result].value >= max_imal:
                    max_imal = network[-1][result].value
            response += str(bot_id) + '=' + str(max_imal) + ','
        elif 'DEAD' in input.split('=')[1]:
            NETWORKS[bot_id].fitness = int(input.split('=')[1].split(':')[1])
    if len(response) > 0:
        if response[-1] == ',':
            response = response[:-1]
    return response


def create_first_generation():  # create the first generation of the networks
    global NETWORKS
    NETWORKS = []
    for i in xrange(CHILDREN):
        new_network = initialize_network(NEURONS)
        NETWORKS.append(make_network_change(new_network.layers, create_random_change(new_network.layers)))


def handle_client_request(resource, client_socket):
    global GEN_COUNTER
    global LAST_RESOURCE
    """ Check the required resource, generate proper HTTP response and send to client"""
    # TO DO : add code that given a resource (URL and parameters) generates the proper response
    status_code = '200 OK'
    http_response = ''
    if resource == '/favicon.ico':  # I am not sure what I am supposed to do with that.
        data = ''
    elif resource == '' or resource == '/':  # if the given url is null (empty):
        GEN_COUNTER = 0
        url = DEFAULT_URL   # then enter a default thing
        data = get_file_data(url)
        http_header = VERSION + ' ' + status_code + '\r\n'
        http_header += 'Content-Length:' + str(len(str(data))) + '\r\n'
        http_header += 'Content-Type:text/html; charset=utf 8\r\n'
        http_header += 'Connection:close\r\n\r\n'
        http_response = http_header + str(data)
    elif resource[1] == 'K':
        resource = resource[2:]     # skip the / in the beginning of the url, as it just marks the beginning of the url
        if 'TERMINATE' in resource:
            if LAST_RESOURCE == 'KTERMINATE':
                data = ''
                print 'I am stuck copying'
            elif GEN_COUNTER == 0:    # if it's the request for the first generation
                create_first_generation()
                data = str(len(NETWORKS))
            elif GEN_COUNTER < GENERATIONS:   # once a round is over, but the evolution hasn't ended yet,
                make_generations()   # create the next generation
                print GEN_COUNTER
                data = str(len(NETWORKS))    # and then send the server how many more players to create
            else:
                #print "Finished ! Here is the text presentation of the best network developed:"
                #print translate_into_text(arrange_by_fitness(NETWORKS)[0])  # print best network
                #print 'the fitness of the network is :' + str(arrange_by_fitness((NETWORKS))[0].fitness)
                data = 'STOP'
            GEN_COUNTER += 1
            LAST_RESOURCE = resource
        elif 'REVEAL' in resource:
            net = int(resource.split('=')[1])
            print translate_into_text(NETWORKS[net])
            data = ''
        else:
            data = play_turn(resource)
    http_response = VERSION + ' ' + status_code + '\r\nContent-Length:' + str(len(str(data))) + '\r\nConnection:close\r\n\r\n' + str(data)
    #   print http_response
    client_socket.send(http_response)


def is_url(str_url):
    if str_url[0] == '/':   # if the first character is '/'
        return True
    else:
        return False


def validate_http_request(request):
    """ Check if request is a valid HTTP request and returns TRUE / FALSE and the requested URL """
    if request[GET_INDEX] == "GET" and is_url(request[URL_INDEX]) and VERSION in request[VERSION_INDEX]:
        return True, request[URL_INDEX]
    else:
        return False, 'error'


def handle_client(client_socket):
    global LAST_RESOURCE
    """ Handles client requests: verifies client's requests are legal HTTP, calls function to handle the requests """
    client_request = client_socket.recv(RECEIVE_LENGTH)
    if len(client_request) > 0:     # if the request isn't empty
        client_request = client_request.replace('\r\n', ' ')
        client_request = client_request.split(" ")
        valid_http, resource = validate_http_request(client_request)
        if valid_http:
            handle_client_request(resource, client_socket)
        else:
            print 'Error: Not a valid HTTP request'
    else:
        print 'request is empty'
    client_socket.close()


def main():
    # Open a socket and loop forever while waiting for clients
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((IP, PORT))
    server_socket.listen(MAX_CONNECTIONS)
    print "Listening for connections on port %d" % PORT
    while True:
        client_socket, client_address = server_socket.accept()
        #   print 'New connection received'
        client_socket.settimeout(SOCKET_TIMEOUT)
        handle_client(client_socket)
    server_socket.close()

if __name__ == "__main__":
    # Call the main handler function
    main()
